const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose'); // to connect to database
const viewersRoutes = require('./routes/Viewer')

const app = express();

app.use(cors()); // to permit all incoming request

/*conect to mongodb atlas via mongoose */
mongoose.connect('mongodb+srv://admin:graplerchina213@wdc028-course-booking.yxol4.mongodb.net/dbBooking_app?retryWrites=true&w=majority', 
{
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindandModify: false


}); 



// to check the connection if success or failure
let db = mongoose.connection;
//if connection error encountered, output it in console
db.on('error', (console.error.bind(console, 'connection error: ')));

//once connection, show notification
db.once('open', () => console.log("We're connected to our database."))


/*Middlewar - built -in function ng app natin*/
app.use(express.json());
//it allows other data types to be used in the body.	
app.use(express.urlencoded( {extended:true}));


//define Schema - structure of documents
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
})

const Task = mongoose.model('Task', taskSchema);


//register a new user

//MVC = model(blueprints), views(user interface), constrollers(Logic)
//new keyword


/*
create a user schema using mongoose with the following fields
username: string
email:string
password:

// task is an array of embedded task documents, it refers to taskSchema 
tasks:array

*/


const userSchema = mongoose.Schema({
     username: String,
     email: String,
     password: String,
     tasks: [taskSchema]
   

})
const User = new mongoose.model('user', userSchema);

app.post('/user', (req, res)=> {
  //check if username or email are duplicate prior to registration
  User.find({ $or: [{ username: req.body.username }, { email: req.body.email }] }, (findErr, duplicates) => {
      if(findErr) return console.error(findErr);
      //if duplicates found
      if(duplicates.length > 0) {
        return res.status(403).json({
          message: "Duplicates found, kindly choose different username and/or email."
        })

      } else {
      //instantiate a new user object with properties derived from the request body
      let newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        //tasks is initially an empty array
        tasks: []
      })
      //save
      newUser.save((saveErr, newUser) => {
        //if an error was ecncounter while saving the document - notification in the console
        if(saveErr) return console.error(saveErr);
        return res.status(201).json({
            message: `User ${newUser.username} successfull registered,`,
            data: {
              username: newUser.username,
              email: newUser.email,
              link_to_self: `/user/${newUser._id}`
            }
        });
      })
    }
  })  
});

//display user
app.get ('/user/:id', (req, res) => {
   User.findById(req.params.id, (err,user) => {
   	if(err) return console.error(err);
   	return res.status(200).json({
   		message: "User retrieved successfully.",
   		data: {
   			username: user.username,
   			email: user.email,
   			tasks: `/user/${user._id}/tasks`
        }
   	 })
   })
})







//request 
/*
<body>
  <form>
    <input name ="username" value="thonie">
    <input name ="email" value ="thonie@yahoo.com">
  </form>
</body>    


app.get('/', (req,res) => {
	{
		username:req.body.username


	},
	{
		email: req.body.email
	}
})

*/






/*Routes*/
app.use('/', viewersRoutes);


///create porn - communication - 3000// by default

const port = 3000;

app.listen(port, () => {console.log(`
	Listening on port ${port}.`)})

